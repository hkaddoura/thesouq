<?php
namespace CheckoutCom\Magento2\Controller\Account\SaveCard;

/**
 * Interceptor class for @see \CheckoutCom\Magento2\Controller\Account\SaveCard
 */
class Interceptor extends \CheckoutCom\Magento2\Controller\Account\SaveCard implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\Controller\Result\JsonFactory $jsonFactory, \Magento\Framework\UrlInterface $urlInterface, \CheckoutCom\Magento2\Model\Service\VaultHandlerService $vaultHandler)
    {
        $this->___init();
        parent::__construct($context, $jsonFactory, $urlInterface, $vaultHandler);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
