<?php
namespace CheckoutCom\Magento2\Controller\Api\V1;

/**
 * Interceptor class for @see \CheckoutCom\Magento2\Controller\Api\V1
 */
class Interceptor extends \CheckoutCom\Magento2\Controller\Api\V1 implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository, \Magento\Quote\Model\QuoteManagement $quoteManagement, \CheckoutCom\Magento2\Gateway\Config\Config $config, \CheckoutCom\Magento2\Helper\Logger $logger, \CheckoutCom\Magento2\Model\Service\QuoteHandlerService $quoteHandler)
    {
        $this->___init();
        parent::__construct($context, $customerRepository, $quoteManagement, $config, $logger, $quoteHandler);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
