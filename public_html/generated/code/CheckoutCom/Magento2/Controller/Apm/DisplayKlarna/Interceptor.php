<?php
namespace CheckoutCom\Magento2\Controller\Apm\DisplayKlarna;

/**
 * Interceptor class for @see \CheckoutCom\Magento2\Controller\Apm\DisplayKlarna
 */
class Interceptor extends \CheckoutCom\Magento2\Controller\Apm\DisplayKlarna implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Framework\Controller\Result\JsonFactory $jsonFactory, \CheckoutCom\Magento2\Gateway\Config\Config $config, \CheckoutCom\Magento2\Model\Service\ApiHandlerService $apiHandler, \CheckoutCom\Magento2\Model\Service\QuoteHandlerService $quoteHandler, \CheckoutCom\Magento2\Model\Service\ShopperHandlerService $shopperHandler, \CheckoutCom\Magento2\Helper\Logger $logger)
    {
        $this->___init();
        parent::__construct($context, $storeManager, $jsonFactory, $config, $apiHandler, $quoteHandler, $shopperHandler, $logger);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
