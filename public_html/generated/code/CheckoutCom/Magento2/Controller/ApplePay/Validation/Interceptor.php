<?php
namespace CheckoutCom\Magento2\Controller\ApplePay\Validation;

/**
 * Interceptor class for @see \CheckoutCom\Magento2\Controller\ApplePay\Validation
 */
class Interceptor extends \CheckoutCom\Magento2\Controller\ApplePay\Validation implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\Controller\Result\RawFactory $rawFactory, \Magento\Framework\HTTP\Client\Curl $curl, \CheckoutCom\Magento2\Gateway\Config\Config $config)
    {
        $this->___init();
        parent::__construct($context, $rawFactory, $curl, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
