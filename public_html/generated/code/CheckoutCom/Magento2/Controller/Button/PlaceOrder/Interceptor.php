<?php
namespace CheckoutCom\Magento2\Controller\Button\PlaceOrder;

/**
 * Interceptor class for @see \CheckoutCom\Magento2\Controller\Button\PlaceOrder
 */
class Interceptor extends \CheckoutCom\Magento2\Controller\Button\PlaceOrder implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Framework\Controller\Result\JsonFactory $jsonFactory, \Magento\Catalog\Model\Product $productModel, \Magento\InstantPurchase\Model\QuoteManagement\ShippingConfiguration $shippingConfiguration, \Magento\Customer\Model\Address $addressManager, \CheckoutCom\Magento2\Model\Service\QuoteHandlerService $quoteHandler, \CheckoutCom\Magento2\Model\Service\OrderHandlerService $orderHandler, \CheckoutCom\Magento2\Model\Service\MethodHandlerService $methodHandler, \CheckoutCom\Magento2\Model\Service\ApiHandlerService $apiHandler, \CheckoutCom\Magento2\Helper\Utilities $utilities, \CheckoutCom\Magento2\Model\InstantPurchase\ShippingSelector $shippingSelector)
    {
        $this->___init();
        parent::__construct($context, $storeManager, $jsonFactory, $productModel, $shippingConfiguration, $addressManager, $quoteHandler, $orderHandler, $methodHandler, $apiHandler, $utilities, $shippingSelector);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
