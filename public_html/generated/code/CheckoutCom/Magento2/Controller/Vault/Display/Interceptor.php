<?php
namespace CheckoutCom\Magento2\Controller\Vault\Display;

/**
 * Interceptor class for @see \CheckoutCom\Magento2\Controller\Vault\Display
 */
class Interceptor extends \CheckoutCom\Magento2\Controller\Vault\Display implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $pageFactory, \Magento\Framework\Controller\Result\JsonFactory $jsonFactory, \CheckoutCom\Magento2\Gateway\Config\Config $config, \CheckoutCom\Magento2\Model\Service\VaultHandlerService $vaultHandler)
    {
        $this->___init();
        parent::__construct($context, $pageFactory, $jsonFactory, $config, $vaultHandler);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
