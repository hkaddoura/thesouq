<?php
namespace CheckoutCom\Magento2\Controller\Webhook\Callback;

/**
 * Interceptor class for @see \CheckoutCom\Magento2\Controller\Webhook\Callback
 */
class Interceptor extends \CheckoutCom\Magento2\Controller\Webhook\Callback implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Sales\Api\OrderRepositoryInterface $orderRepository, \CheckoutCom\Magento2\Model\Service\ApiHandlerService $apiHandler, \CheckoutCom\Magento2\Model\Service\OrderHandlerService $orderHandler, \CheckoutCom\Magento2\Model\Service\QuoteHandlerService $quoteHandler, \CheckoutCom\Magento2\Model\Service\ShopperHandlerService $shopperHandler, \CheckoutCom\Magento2\Model\Service\TransactionHandlerService $transactionHandler, \CheckoutCom\Magento2\Model\Service\VaultHandlerService $vaultHandler, \CheckoutCom\Magento2\Model\Service\PaymentErrorHandlerService $paymentErrorHandler, \CheckoutCom\Magento2\Gateway\Config\Config $config, \CheckoutCom\Magento2\Helper\Utilities $utilities)
    {
        $this->___init();
        parent::__construct($context, $storeManager, $orderRepository, $apiHandler, $orderHandler, $quoteHandler, $shopperHandler, $transactionHandler, $vaultHandler, $paymentErrorHandler, $config, $utilities);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
