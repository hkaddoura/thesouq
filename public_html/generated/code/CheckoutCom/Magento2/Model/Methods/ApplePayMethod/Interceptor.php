<?php
namespace CheckoutCom\Magento2\Model\Methods\ApplePayMethod;

/**
 * Interceptor class for @see \CheckoutCom\Magento2\Model\Methods\ApplePayMethod
 */
class Interceptor extends \CheckoutCom\Magento2\Model\Methods\ApplePayMethod implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\Model\Context $context, \Magento\Framework\Registry $registry, \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory, \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory, \Magento\Payment\Helper\Data $paymentData, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Payment\Model\Method\Logger $logger, \Magento\Backend\Model\Auth\Session $backendAuthSession, \Magento\Checkout\Model\Cart $cart, \Magento\Framework\UrlInterface $urlBuilder, \Magento\Framework\ObjectManagerInterface $objectManager, \Magento\Sales\Model\Order\Email\Sender\InvoiceSender $invoiceSender, \Magento\Framework\DB\TransactionFactory $transactionFactory, \Magento\Customer\Model\Session $customerSession, \Magento\Checkout\Model\Session $checkoutSession, \Magento\Checkout\Helper\Data $checkoutData, \Magento\Quote\Api\CartRepositoryInterface $quoteRepository, \Magento\Quote\Api\CartManagementInterface $quoteManagement, \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender, \Magento\Backend\Model\Session\Quote $sessionQuote, \CheckoutCom\Magento2\Gateway\Config\Config $config, \CheckoutCom\Magento2\Model\Service\ApiHandlerService $apiHandler, \CheckoutCom\Magento2\Helper\Utilities $utilities, \Magento\Store\Model\StoreManagerInterface $storeManager, \CheckoutCom\Magento2\Helper\Logger $ckoLogger, \CheckoutCom\Magento2\Model\Service\QuoteHandlerService $quoteHandler, ?\Magento\Framework\Model\ResourceModel\AbstractResource $resource = null, ?\Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null, array $data = [])
    {
        $this->___init();
        parent::__construct($context, $registry, $extensionFactory, $customAttributeFactory, $paymentData, $scopeConfig, $logger, $backendAuthSession, $cart, $urlBuilder, $objectManager, $invoiceSender, $transactionFactory, $customerSession, $checkoutSession, $checkoutData, $quoteRepository, $quoteManagement, $orderSender, $sessionQuote, $config, $apiHandler, $utilities, $storeManager, $ckoLogger, $quoteHandler, $resource, $resourceCollection, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function denyPayment(\Magento\Payment\Model\InfoInterface $payment)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'denyPayment');
        if (!$pluginInfo) {
            return parent::denyPayment($payment);
        } else {
            return $this->___callPlugins('denyPayment', func_get_args(), $pluginInfo);
        }
    }
}
