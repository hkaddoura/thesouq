<?php
namespace Magento\Customer\Api\Data;

/**
 * Extension class for @see \Magento\Customer\Api\Data\AddressInterface
 */
class AddressExtension extends \Magento\Framework\Api\AbstractSimpleObject implements AddressExtensionInterface
{
    /**
     * @return int|null
     */
    public function getQuickbooksId()
    {
        return $this->_get('quickbooks_id');
    }

    /**
     * @param int $quickbooksId
     * @return $this
     */
    public function setQuickbooksId($quickbooksId)
    {
        $this->setData('quickbooks_id', $quickbooksId);
        return $this;
    }
}
