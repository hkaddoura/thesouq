<?php
namespace Magento\Customer\Api\Data;

/**
 * ExtensionInterface class for @see \Magento\Customer\Api\Data\AddressInterface
 */
interface AddressExtensionInterface extends \Magento\Framework\Api\ExtensionAttributesInterface
{
    /**
     * @return int|null
     */
    public function getQuickbooksId();

    /**
     * @param int $quickbooksId
     * @return $this
     */
    public function setQuickbooksId($quickbooksId);
}
