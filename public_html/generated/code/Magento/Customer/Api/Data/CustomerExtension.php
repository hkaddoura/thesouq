<?php
namespace Magento\Customer\Api\Data;

/**
 * Extension class for @see \Magento\Customer\Api\Data\CustomerInterface
 */
class CustomerExtension extends \Magento\Framework\Api\AbstractSimpleObject implements CustomerExtensionInterface
{
    /**
     * @return boolean|null
     */
    public function getIsSubscribed()
    {
        return $this->_get('is_subscribed');
    }

    /**
     * @param boolean $isSubscribed
     * @return $this
     */
    public function setIsSubscribed($isSubscribed)
    {
        $this->setData('is_subscribed', $isSubscribed);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAmazonId()
    {
        return $this->_get('amazon_id');
    }

    /**
     * @param string $amazonId
     * @return $this
     */
    public function setAmazonId($amazonId)
    {
        $this->setData('amazon_id', $amazonId);
        return $this;
    }

    /**
     * @return int|null
     */
    public function getQuickbooksId()
    {
        return $this->_get('quickbooks_id');
    }

    /**
     * @param int $quickbooksId
     * @return $this
     */
    public function setQuickbooksId($quickbooksId)
    {
        $this->setData('quickbooks_id', $quickbooksId);
        return $this;
    }

    /**
     * @return int|null
     */
    public function getQuickbooksSync()
    {
        return $this->_get('quickbooks_sync');
    }

    /**
     * @param int $quickbooksSync
     * @return $this
     */
    public function setQuickbooksSync($quickbooksSync)
    {
        $this->setData('quickbooks_sync', $quickbooksSync);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getVertexCustomerCode()
    {
        return $this->_get('vertex_customer_code');
    }

    /**
     * @param string $vertexCustomerCode
     * @return $this
     */
    public function setVertexCustomerCode($vertexCustomerCode)
    {
        $this->setData('vertex_customer_code', $vertexCustomerCode);
        return $this;
    }
}
