<?php
namespace Meetanshi\Mobilelogin\Controller\Account\Create;

/**
 * Interceptor class for @see \Meetanshi\Mobilelogin\Controller\Account\Create
 */
class Interceptor extends \Meetanshi\Mobilelogin\Controller\Account\Create implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Meetanshi\Mobilelogin\Helper\Data $helper)
    {
        $this->___init();
        parent::__construct($context, $helper);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
