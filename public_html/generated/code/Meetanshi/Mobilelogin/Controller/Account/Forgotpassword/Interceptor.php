<?php
namespace Meetanshi\Mobilelogin\Controller\Account\Forgotpassword;

/**
 * Interceptor class for @see \Meetanshi\Mobilelogin\Controller\Account\Forgotpassword
 */
class Interceptor extends \Meetanshi\Mobilelogin\Controller\Account\Forgotpassword implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context)
    {
        $this->___init();
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
