<?php
namespace Meetanshi\Mobilelogin\Controller\Adminhtml\Otp\Developer;

/**
 * Interceptor class for @see \Meetanshi\Mobilelogin\Controller\Adminhtml\Otp\Developer
 */
class Interceptor extends \Meetanshi\Mobilelogin\Controller\Adminhtml\Otp\Developer implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Meetanshi\Mobilelogin\Helper\Data $helper, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory)
    {
        $this->___init();
        parent::__construct($context, $helper, $resultJsonFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
