<?php
namespace NS8\CSP\Controller\Adminhtml\Ajax\Validate;

/**
 * Interceptor class for @see \NS8\CSP\Controller\Adminhtml\Ajax\Validate
 */
class Interceptor extends \NS8\CSP\Controller\Adminhtml\Ajax\Validate implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \NS8\CSP\Helper\Config $configHelper, \NS8\CSP\Helper\Order $orderHelper, \NS8\CSP\Helper\Logger $logger, \NS8\CSP\Helper\RESTClient $restClient, \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory)
    {
        $this->___init();
        parent::__construct($context, $configHelper, $orderHelper, $logger, $restClient, $customerRepositoryInterface, $resultJsonFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
