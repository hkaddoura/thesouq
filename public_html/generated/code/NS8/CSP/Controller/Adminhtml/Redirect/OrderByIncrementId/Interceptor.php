<?php
namespace NS8\CSP\Controller\Adminhtml\Redirect\OrderByIncrementId;

/**
 * Interceptor class for @see \NS8\CSP\Controller\Adminhtml\Redirect\OrderByIncrementId
 */
class Interceptor extends \NS8\CSP\Controller\Adminhtml\Redirect\OrderByIncrementId implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \NS8\CSP\Helper\Order $orderHelper, \NS8\CSP\Helper\Config $configHelper)
    {
        $this->___init();
        parent::__construct($context, $resultPageFactory, $orderHelper, $configHelper);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
