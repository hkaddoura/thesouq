<?php
namespace Paymentwall\Paymentwall\Controller\Index\Pingback;

/**
 * Interceptor class for @see \Paymentwall\Paymentwall\Controller\Index\Pingback
 */
class Interceptor extends \Paymentwall\Paymentwall\Controller\Index\Pingback implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $pageFactory, \Paymentwall\Paymentwall\Model\Pingback $modelPingback)
    {
        $this->___init();
        parent::__construct($context, $pageFactory, $modelPingback);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
