<?php
namespace Paysera\Magento2Paysera\Controller\Index\Index;

/**
 * Interceptor class for @see \Paysera\Magento2Paysera\Controller\Index\Index
 */
class Interceptor extends \Paysera\Magento2Paysera\Controller\Index\Index implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $pageFactory, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Checkout\Model\Session $checkoutSession, \Paysera\Magento2Paysera\Helper\Data $helper)
    {
        $this->___init();
        parent::__construct($context, $pageFactory, $scopeConfig, $storeManager, $checkoutSession, $helper);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
