<?php
namespace TNW\QuickbooksBasic\Controller\Adminhtml\Connect\Index;

/**
 * Interceptor class for @see \TNW\QuickbooksBasic\Controller\Adminhtml\Connect\Index
 */
class Interceptor extends \TNW\QuickbooksBasic\Controller\Adminhtml\Connect\Index implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, \TNW\QuickbooksBasic\Model\Quickbooks $quickbooks)
    {
        $this->___init();
        parent::__construct($context, $resultJsonFactory, $quickbooks);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
