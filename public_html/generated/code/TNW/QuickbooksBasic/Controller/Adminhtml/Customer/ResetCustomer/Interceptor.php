<?php
namespace TNW\QuickbooksBasic\Controller\Adminhtml\Customer\ResetCustomer;

/**
 * Interceptor class for @see \TNW\QuickbooksBasic\Controller\Adminhtml\Customer\ResetCustomer
 */
class Interceptor extends \TNW\QuickbooksBasic\Controller\Adminhtml\Customer\ResetCustomer implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Customer\Model\ResourceModel\Customer $resourceCustomer, \Magento\Framework\DataObject\Factory $dataObjectFactory)
    {
        $this->___init();
        parent::__construct($context, $resourceCustomer, $dataObjectFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
