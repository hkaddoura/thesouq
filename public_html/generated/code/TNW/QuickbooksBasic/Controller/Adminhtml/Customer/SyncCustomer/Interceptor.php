<?php
namespace TNW\QuickbooksBasic\Controller\Adminhtml\Customer\SyncCustomer;

/**
 * Interceptor class for @see \TNW\QuickbooksBasic\Controller\Adminhtml\Customer\SyncCustomer
 */
class Interceptor extends \TNW\QuickbooksBasic\Controller\Adminhtml\Customer\SyncCustomer implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \TNW\QuickbooksBasic\Model\Quickbooks\SyncManager $syncManager, \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository)
    {
        $this->___init();
        parent::__construct($context, $syncManager, $customerRepository);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
