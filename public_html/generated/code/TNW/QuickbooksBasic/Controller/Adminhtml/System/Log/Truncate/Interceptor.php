<?php
namespace TNW\QuickbooksBasic\Controller\Adminhtml\System\Log\Truncate;

/**
 * Interceptor class for @see \TNW\QuickbooksBasic\Controller\Adminhtml\System\Log\Truncate
 */
class Interceptor extends \TNW\QuickbooksBasic\Controller\Adminhtml\System\Log\Truncate implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \TNW\QuickbooksBasic\Model\ResourceModel\Message $resourceLogger)
    {
        $this->___init();
        parent::__construct($context, $resourceLogger);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
