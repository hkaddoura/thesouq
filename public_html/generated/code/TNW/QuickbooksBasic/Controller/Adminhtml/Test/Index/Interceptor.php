<?php
namespace TNW\QuickbooksBasic\Controller\Adminhtml\Test\Index;

/**
 * Interceptor class for @see \TNW\QuickbooksBasic\Controller\Adminhtml\Test\Index
 */
class Interceptor extends \TNW\QuickbooksBasic\Controller\Adminhtml\Test\Index implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, \TNW\QuickbooksBasic\Model\Quickbooks $quickbooks, \TNW\QuickbooksBasic\Model\Quickbooks\Company $quickbooksCompany)
    {
        $this->___init();
        parent::__construct($context, $resultJsonFactory, $quickbooks, $quickbooksCompany);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
