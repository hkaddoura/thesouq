<?php
namespace TNW\QuickbooksBasic\Model\Config\Backend\Environment;

/**
 * Interceptor class for @see \TNW\QuickbooksBasic\Model\Config\Backend\Environment
 */
class Interceptor extends \TNW\QuickbooksBasic\Model\Config\Backend\Environment implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\Model\Context $context, \Magento\Framework\Registry $registry, \Magento\Framework\App\Config\ScopeConfigInterface $config, \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList, \Magento\Config\Model\ResourceModel\Config $resourceConfig, \TNW\QuickbooksBasic\Model\Config $quickbooksConfig, ?\Magento\Framework\Model\ResourceModel\AbstractResource $resource = null, ?\Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null, array $data = [])
    {
        $this->___init();
        parent::__construct($context, $registry, $config, $cacheTypeList, $resourceConfig, $quickbooksConfig, $resource, $resourceCollection, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'afterSave');
        if (!$pluginInfo) {
            return parent::afterSave();
        } else {
            return $this->___callPlugins('afterSave', func_get_args(), $pluginInfo);
        }
    }
}
