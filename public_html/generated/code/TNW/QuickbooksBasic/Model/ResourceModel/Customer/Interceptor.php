<?php
namespace TNW\QuickbooksBasic\Model\ResourceModel\Customer;

/**
 * Interceptor class for @see \TNW\QuickbooksBasic\Model\ResourceModel\Customer
 */
class Interceptor extends \TNW\QuickbooksBasic\Model\ResourceModel\Customer implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\ResourceConnection\ConfigInterface $resourceConfig, \Magento\Framework\Model\ResourceModel\Type\Db\ConnectionFactoryInterface $connectionFactory, \Magento\Framework\App\DeploymentConfig $deploymentConfig, \TNW\QuickbooksBasic\Model\Customer\CustomAttribute $customAttribute, $tablePrefix = '')
    {
        $this->___init();
        parent::__construct($resourceConfig, $connectionFactory, $deploymentConfig, $customAttribute, $tablePrefix);
    }

    /**
     * {@inheritdoc}
     */
    public function getTableName($modelEntity, $connectionName = 'default')
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getTableName');
        if (!$pluginInfo) {
            return parent::getTableName($modelEntity, $connectionName);
        } else {
            return $this->___callPlugins('getTableName', func_get_args(), $pluginInfo);
        }
    }
}
