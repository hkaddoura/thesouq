    var ctx = require.s.contexts._,
        origNameToUrl = ctx.nameToUrl;

    ctx.nameToUrl = function() {
        var url = origNameToUrl.apply(ctx, arguments);
        if (!url.match(/\/tiny_mce\//)&&!url.match(/\/v1\/songbird/)&&!url.match(/\.authorize\.net\/v1\/Accept/)&&!url.match(/https:\/\/cdn.checkout.com\/js\/framesv2.min.js/)&&!url.match(/https:\/\/x.klarnacdn.net\/kp\/lib\/v1\/api.js/)&&!url.match(/https:\/\/pay.google.com\/gp\/p\/js\/pay.js/)&&!url.match(/\/Temando_Shipping\/static\/js\//)&&!url.match(/https:\/\/www.google.com\/recaptcha\/api.js/)) {
            url = url.replace(/(\.min)?\.js$/, '.min.js');
        }
        return url;
    };
