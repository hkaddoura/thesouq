<?php
namespace Meetanshi\Mobilelogin\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class Onepage extends Template
{
    public function __construct(Context $context)
    {
        parent::__construct($context);
    }
}
