<?php

namespace Meetanshi\Mobilelogin\Block\System\Config;

class Synchronize extends \Magento\Config\Block\System\Config\Form\Field
{
    protected $_template = 'Meetanshi_Mobilelogin::system/config/synchronize.phtml';

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        array $data = []
    )
    {
        parent::__construct($context, $data);
    }

    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }

    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        return $this->_toHtml();
    }

    public function getAjaxUrl()
    {
        return $this->getUrl('mobilelogin/otp/synchronize');
    }

    public function getButtonHtml()
    {
        $button = $this->getLayout()->createBlock(
            'Magento\Backend\Block\Widget\Button'
        )->setData(
            [
                'id' => 'synchronize',
                'label' => __('Sync Now'),
            ]
        );

        return $button->toHtml();
    }
}