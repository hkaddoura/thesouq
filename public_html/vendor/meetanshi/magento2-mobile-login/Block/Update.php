<?php

namespace Meetanshi\Mobilelogin\Block;

use Magento\Framework\View\Element\Template;
use Meetanshi\Mobilelogin\Helper\Data;
use \Magento\Framework\Serialize\Serializer\Json;

class Update extends Template
{
    private $helper;
    protected $jsonHelper;

    public function __construct(
        Template\Context $context,
        Json $jsonHelper,
        Data $helper
    ) {
    
        $this->helper = $helper;
        $this->jsonHelper = $jsonHelper;
        parent::__construct($context);
    }

    public function getCustomerMobileNumber()
    {
        return $this->helper->getCustomerMobile();
    }

    public function phoneConfig()
    {
        $config  = [
            "nationalMode" => false,
            "utilsScript"  => $this->getViewFileUrl('Meetanshi_Mobilelogin::js/utils.js'),
            "preferredCountries" => [$this->helper->preferedCountry()]
        ];

        if ($this->helper->allowedCountries()) {
            $config["onlyCountries"] = explode(",", $this->helper->allowedCountries());
        }

        return $this->jsonHelper->serialize($config);
    }
}
