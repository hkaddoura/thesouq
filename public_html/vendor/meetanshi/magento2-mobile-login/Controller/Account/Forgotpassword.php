<?php

namespace Meetanshi\Mobilelogin\Controller\Account;

use Magento\Customer\Controller\AbstractAccount;

class Forgotpassword extends AbstractAccount
{
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}
