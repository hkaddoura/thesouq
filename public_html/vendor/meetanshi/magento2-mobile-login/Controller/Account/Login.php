<?php

namespace Meetanshi\Mobilelogin\Controller\Account;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Meetanshi\Mobilelogin\Helper\Data;

class Login extends Action
{
    private $helper;

    public function __construct(Context $context, Data $helper
    )
    {
        $this->helper = $helper;
        parent::__construct($context);
    }

    public function execute()
    {
        return $this->helper->loginPost($this->getRequest()->getParams());
    }
}
