<?php

namespace Meetanshi\Mobilelogin\Controller\Otp;

use Magento\Customer\Controller\AbstractAccount;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Meetanshi\Mobilelogin\Helper\Data;
use Magento\Store\Model\StoreManagerInterface;

class Resetpassword extends AbstractAccount
{
    private $helper;
    protected $mobileloginFactory;
    private $jsonFactory;
    protected $storeManager;

    public function __construct(
        Context $context,
        Data $helperData,
        JsonFactory $jsonFactory,
        StoreManagerInterface $storeManager
    ) {
    
        $this->helper = $helperData;
        $this->jsonFactory = $jsonFactory;
        $this->storeManager = $storeManager;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->jsonFactory->create();
        $response = [
            'succeess' => "true",
            'errormsg' => "Something went wrong.",
            'successmesg' => "",
            'customurl' => ''
        ];

        try {
            $param = $this->getRequest()->getParams();

            $customer = $this->helper->getCustomerCollectionMobile($param['mobilenumber']);
            if ($customer->getId()) {
                $customer->setMobileNumber($param['mobilenumber']);
                $customer->setRpToken($customer->getRpToken());
                $customer->setPassword($param['password']);
                $customer->save();
                $response['customurl'] = $this->storeManager->getStore()->getUrl('customer/account/login');
                $response['successmsg'] = 'Password has been changed successfully. You can now login with your new credentials.';
            } else {
                $response['errormsg'] = 'Password change error, please try again.';
                $response['succeess'] = "false";
            }

            $result->setData($response);
            return $result;
        } catch (\Exception $e) {
            $response['errormsg'] = 'Password change error, please try again.';
            $response['succeess'] = "false";
            $result->setData($response);
            return $result;
        }
    }
}
