<?php

namespace Meetanshi\Mobilelogin\Controller\Otp;

use Magento\Framework\App\Action\Action;

class Update extends Action
{
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->getPage()->getConfig()->getTitle()->set(__('Mobile Number'));
        $this->_view->renderLayout();
    }
}
