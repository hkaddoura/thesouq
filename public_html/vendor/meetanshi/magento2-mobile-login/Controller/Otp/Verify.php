<?php

namespace Meetanshi\Mobilelogin\Controller\Otp;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Meetanshi\Mobilelogin\Helper\Data;

class Verify extends Action
{
    private $helper;

    public function __construct(Context $context, Data $helperData)
    {
        $this->helper = $helperData;
        parent::__construct($context);
    }

    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->renderLayout();
        return $this->helper->otpVerify($this->getRequest()->getParams());
    }
}
