<?php

namespace Meetanshi\Mobilelogin\Model\ResourceModel\Mobilelogin;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Meetanshi\Mobilelogin\Model\Mobilelogin',
            'Meetanshi\Mobilelogin\Model\ResourceModel\Mobilelogin'
        );
    }
}
