<?php

namespace Meetanshi\Mobilelogin\Plugin\Customer;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\Account\Redirect as AccountRedirect;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Message\ManagerInterface;
use Meetanshi\Mobilelogin\Helper\Data;

class Loginpost
{
    private $request;
    private $mobileHelper;
    private $customerAccountManagement;
    private $session;
    private $formKeyValidator;
    private $accountRedirect;
    private $resultRedirect;
    private $messageManager;

    public function __construct(
        Http $request,
        Data $helper,
        AccountManagementInterface $customerAccountManagement,
        Session $customerSession,
        Validator $formKeyValidator,
        AccountRedirect $accountRedirect,
        Redirect $redirect,
        ManagerInterface $messageManager
    ) {
    
        $this->mobileHelper = $helper;
        $this->request = $request;
        $this->customerAccountManagement = $customerAccountManagement;
        $this->session = $customerSession;
        $this->formKeyValidator = $formKeyValidator;
        $this->accountRedirect = $accountRedirect;
        $this->resultRedirect = $redirect;
        $this->messageManager = $messageManager;
    }

    public function aroundExecute()
    {
        try {
            $param = $this->request->getParams();
            if ($this->session->isLoggedIn()) {
                $this->resultRedirect->setPath('customer/account');
                return $this->resultRedirect;
            } else {
                $login = $param['login'];
                $username = $login['username'];
                if (is_numeric($login['username'])) {
                    $username = $this->mobileHelper->getCustomerCollectionMobile($username)->getEmail();
                }
                if (!empty($login['username']) && !empty($login['password'])) {
                    $customer = $this->customerAccountManagement->authenticate($username, $login['password']);
                    $this->session->setCustomerDataAsLoggedIn($customer);
                    $this->session->regenerateId();
                }
                $this->resultRedirect->setPath('customer/account');
                return $this->resultRedirect;
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__("Invalid Username or Password"));
            $this->resultRedirect->setPath('*/*/');
            return $this->resultRedirect;
        }
    }
}
