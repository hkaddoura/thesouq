<?php
namespace Paysera\Magento2Paysera\Controller\Index;

use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Checkout\Model\Session;
use Paysera\Magento2Paysera\Helper\Data;
use WebToPay;

class Index extends \Magento\Framework\App\Action\Action
{
    const PAYSERA_PAYMENT  = 'payment/paysera';
    const PAYSERA_STATUS   = 'paysera_order_status';
    const ORDER_STATUS     = 'pending_payment_order_status';
    const SUCCESS_ADDRESS  = 'checkout/onepage/success?paysera';
    const CALLBACK_ADDRESS = 'paysera/index/callback';

    const PAYSERA_TYPE     = 'paysera_payment_type';
    const REDIRECT         = 'pageRedirectUrl';

    const DEFAULT_LANG     = 'ENG';

    const LOCALE_CODE      =  'general/locale/code';

    protected $_pageFactory;
    protected $_checkoutSession;
    protected $_scopeConfig;
    protected $_storeManager;
    protected $_helper;

    protected $payseraLangs;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        Session $checkoutSession,
        Data $helper
    ) {
        $this->_pageFactory     = $pageFactory;
        $this->_scopeConfig     = $scopeConfig;
        $this->_storeManager    = $storeManager;
        $this->_checkoutSession = $checkoutSession;
        $this->_helper          = $helper;

        $this->payseraLangs = [
            'lt' => 'LIT',
            'lv' => 'LAV',
            'et' => 'EST',
            'ru' => 'RUS',
            'de' => 'GER',
            'pl' => 'POL',
            'en' => 'ENG',
        ];

        return parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     *
     * @throws \WebToPayException|\Exception
     */
    public function execute()
    {
        $order = $this->_checkoutSession->getLastRealOrder();

        $paysera_config = $this->_scopeConfig->getValue(
            self::PAYSERA_PAYMENT,
            ScopeInterface::SCOPE_STORE
        );

        $order->setStatus(
            $paysera_config[self::PAYSERA_STATUS][self::ORDER_STATUS]
        )->save();

        $payment = $this->getDataFromSession(self::PAYSERA_TYPE);

        print_r($this->buildRedirectLink(
            $this->getPayseraPaymentUrl($payment)
        ));
    }

    /**
     * @param string $link
     *
     * @return false|string
     */
    protected function buildRedirectLink($link)
    {
        $parameter = ['url' => $link,];

        return json_encode($parameter);
    }

    /**
     * @param string $name
     *
     * @return string
     */
    protected function getDataFromSession($name)
    {
        return $this->_helper->getSessionData($name);
    }

    /**
     * @return string
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getBasePage()
    {
        return $this->_helper->getPageBaseUrl();
    }

    /**
     * @param string $payment
     * @return string
     *
     * @throws \WebToPayException|\Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getPayseraPaymentUrl($payment)
    {
        $order = $this->_checkoutSession->getLastRealOrder();

        $paysera_config = $this->_scopeConfig->getValue(
            self::PAYSERA_PAYMENT,
            ScopeInterface::SCOPE_STORE
        );

        $selectedLanguage = $this->_scopeConfig->getValue(
            self::LOCALE_CODE,
            ScopeInterface::SCOPE_STORE
        );

        $selectedLanguageCode = substr($selectedLanguage,0,2);
        if ($this->payseraLangs[$selectedLanguageCode]) {
            $lang = $this->payseraLangs[$selectedLanguageCode];
        } else {
            $lang = self::DEFAULT_LANG;
        }

        $buildParameters = [
            'projectid'     => $paysera_config['projectid'],
            'sign_password' => $paysera_config['sign_password'],

            'orderid'       => $order->getId(),
            'amount'        => $order->getGrandTotal() * 100,
            'currency'      => $order->getOrderCurrencyCode(),

            'accepturl'     => $this->getBasePage() . self::SUCCESS_ADDRESS,
            'callbackurl'   => $this->getBasePage() . self::CALLBACK_ADDRESS,
            'cancelurl'     => $this->getBasePage(),

            'p_firstname'   => $order->getBillingAddress()->getFirstname(),
            'p_lastname'    => $order->getBillingAddress()->getLastname(),
            'p_email'       => $order->getBillingAddress()->getEmail(),
            'p_street'      => $order->getBillingAddress()->getStreet()[0],
            'p_city'        => $order->getBillingAddress()->getCity(),
            'p_state'       => substr($order->getBillingAddress()->getRegion(), 0, 20),
            'p_zip'         => $order->getBillingAddress()->getPostcode(),
            'p_countrycode' => $order->getBillingAddress()->getCountryId(),

            'payment'       => $payment,
            'lang'          => $lang,

            'test'          => $paysera_config['test'],
        ];

        $request = WebToPay::buildRequest($buildParameters);

        $redirectUrl = WebToPay::PAY_URL . '?' . http_build_query($request);

        $redirectUrlResult = preg_replace(
            '/[\r\n]+/is',
            '',
            $redirectUrl
        );

        return $redirectUrlResult;
    }
}
