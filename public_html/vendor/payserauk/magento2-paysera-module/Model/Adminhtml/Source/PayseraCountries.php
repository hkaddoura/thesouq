<?php
namespace Paysera\Magento2Paysera\Model\Adminhtml\Source;

use WebToPay;

class PayseraCountries
{
    /**
     * Default project id
     */
    const DEFAULT_PROJECT_ID = 1;

    /**
     * Default currency
     */
    const DEFAULT_CURRENCY = 'EUR';

    /**
     * Default language
     */
    const DEFAULT_LANG = 'en';

    /**
     * @param string $project
     * @param string $currency
     * @param string $lang
     *
     * @return \WebToPay_PaymentMethodCountry[]
     *
     * @throws \WebToPayException
     */
    public function getPayseraCountries($project, $currency, $lang)
    {
        $countries = WebToPay::getPaymentMethodList(
            $project,
            $currency
        )->setDefaultLanguage(
            $lang
        )->getCountries();

        return $countries;
    }

    /**
     * @param array $countries
     *
     * @return array
     */
    public function getCountriesList($countries)
    {
        $countriesList = [];

        foreach ($countries as $country) {
            $countriesList[] = [
                'value' => $country->getCode(),
                'label' => $country->getTitle()
            ];
        }

        return $countriesList;
    }

    /**
     * @return array
     *
     * @throws \WebToPayException
     */
    public function toOptionArray()
    {
        $payseraCountries = $this->getPayseraCountries(
            self::DEFAULT_PROJECT_ID,
            self::DEFAULT_CURRENCY,
            self::DEFAULT_LANG
        );

        $options = $this->getCountriesList($payseraCountries);

        return $options;
    }
}
